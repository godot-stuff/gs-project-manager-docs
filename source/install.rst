Installation
============
If you are familiar with running Python you should have no problem getting **gspm** installed quickly.

The tool should run on any **Windows**, **Mac** or **Linux** machine, with a version of **Python** of at least **3.4** or greater.

You can use a Virtual Environment if you want to, but it is not a requirement.

Install With Pip
----------------
To install the package using *pip*, you should run the following command.

.. code-block:: bash

    > pip install gspm

If you want to upgrade in the future, you can run this command.

.. code-block:: bash

    > pip install --upgrade gspm

Post Installation
-----------------
To verify you have it installed, simply issue the following command to tell you the version that is running.

.. code-block:: bash
    
    > gspm --version

If things are setup correctly, you should see something similar to below.

.. code-block::

     __ _ ___ _ __  _ __ ___  
    / _` / __| '_ \| '_ ` _ \ 
   | (_| \__ \ |_) | | | | | |
    \__, |___/ .__/|_| |_| |_|
    |___/    |_|              

    godot-stuff Project Manager
    Version 0.1.15, Copyright 2018-2020, SpockerDotNet LLC

Congratulations, you have just made your managing your Godot projects easier.