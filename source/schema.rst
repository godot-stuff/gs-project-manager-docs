YAML Schema Reference
=====================
This is a detailed reference guide for a **gspm** project file.

.. _project-information:

Project Information
-------------------

.. code-block:: yaml

    name: string # required
    description: string  # optional
    default_type: [type] # optional
    version: string #optional

    godot:
        version: [version] # required
        release: [release] # optional
        arch: [arch] # optional
        mono: boolean # optional
        local: string # optional
        location: string # optional (deprecated)

==============  ==============
Option            Description
==============  ==============
*name*          the name of the project
*description*   a short description of the project
*default_type*  the default :ref:`pull-types` for assets
*version*       a string representation of the version of this project
*godot*         godot specifications
==============  ==============


=============== =============== 
Godot Option    Description
=============== ===============
*version*       the :ref:`godot-version` to use when developing
*release*       the :ref`godot-release` to use (ex: beta1, rc1, rc3 and so on, default is blank)
*arch*          32 or 64 bit :ref:`godot-architecture`
*mono*          use the mono build (default is *false*)
*local*         use this local binary instead (will override other options)
*location*      use this local binary instead (will override other options) *deprecated*
=============== ===============

.. _asset-information:

Asset Information
-----------------

.. code-block:: yaml

    assets: # required

    name: # required
        location: uri # required
        type: [type] # optional
        active: boolean # optional
        branch: string # optional
        includes: 
        - dir: string # optional
        todir: string # optional

=============== =============== 
Option          Description
=============== ===============
*name*          the name of the asset
*location*      the `uniform resource identifier <https://en.wikipedia.org/wiki/Uniform_Resource_Identifier>` of the file 
*type*          the type of asset to pull, see :ref:`pull-types`
*active*        should this asset be included (default is *true*)
*branch*        name of the branch to pull (default is *master*)
*includes*      what directory or directories to include
=============== ===============

=============== =============== 
Include Option  Description
=============== ===============
*dir*           name of the directory to include
*todir*         destination directory of include
=============== ===============

Attributes
----------

.. _godot-version:

Godot Version
-------------
You can specify which version of Godot your project is using.

=============== ===============
Value           Description
=============== ===============
*latest*        latest supported version (default)
*x.x*           Major Minor (eg 2.0, 3.0)
*x.x.x*         Major Minor Revision (eg 2.0.1, 3.0.6)
=============== ===============

.. _godot-release:

Godot Release
-------------
You can specify which release of a version of Godot your project is using.

=============== ===============
Value           Description
=============== ===============
*stable*        stable version (default)
*betaX*         beta releases (eg beta1, beta2)
*rcX*           release candidate (eg rc1, rc2, rc3)
=============== ===============

.. _godot-architecture:

Godot Architecture
------------------
Godot is available in either 32 or 64 bit. You can specify which architecture you prefer by adding this attribute.

=============== ===============
Value           Description
=============== ===============
*32*            32 bit version 
*64*            64 bit version (default)
=============== ===============

.. _pull-types:

Pull Types
----------
Each asset is pulled from some type of Source and that Source needs to be identified in some way to determine how to pull it down. This is specified using the Type attribute on each Asset. These are the different types that you can specify.

=============== ===============
Value           Description
=============== ===============
*git*           asset comes from a git repository (default)
*copy*          copy assets from a local folder on your computer
*zip*           extract from a zip file
=============== ===============
