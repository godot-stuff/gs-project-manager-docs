Project
=======
The project package uses a **YAML** file that contains information which defines the components of your project. **YAML** is a very simple file format to read and edit. If you are not familiar with Yaml, please introduce yourself `here <https://yaml.org/>`_. By default, **gspm** will look for a file called *project.yml* in the current directory, however you can override this using the ``--config`` option.

The file has a very simple structure which breaks up the project into two sections. For a more detailed look at the project definition file, see the [Schema Reference page](schema).

Main Section
------------
The first section is used to give some basic information about the project like the name, what version of Godot should be used and so on. In the example below, you can see that this project is using the latest stable 32bit version, and that any asset in the project will default to using **git**. 

.. code-block:: yaml

    name: my-cool-game
    description: My Very Cool Game
    default_type: git
    godot:
      version: 3.3
      arch: 64

Assets Section
--------------
This section contains information about the different Assets to include in your project. Assets can be anything you want, but usually it is just some other source code or maybe another project folder on your computer, or perhaps it is something from the Godot Asset Library. The example below shows some examples of what you can include. The first is used to copy a folder from another project on your computer, the next is used to pull a zip file from the internet, and the last example will pull some code down from a git repository. 

.. code-block:: yaml

    assets:

      some-local-asset:
        location: file://some.path/on.my/desktop
        type: copy
        includes:
        - dir: some/path
        - todir: another/path

      some-zip-asset:
        location: https://some.path/file.zip
        type: zip
        active: false

      some-asset-on-github:
        location: https://github.com/some/asset.git
        includes:
        - dir: addons

Export Section
--------------
This section contains information to assist you when exporting your project from the command line. 

.. code-block:: yaml

  exports:

    windows:
      name: win
      path: ./build/win
      file: test_game.exe

    themac:
      name: mac
      path: ./build/mac
      file: test_game.app

Replacement Tokens
------------------
The tool lets you put replacement tokens inside your `project.yml` file.

This is a convenient way to specify the values for options that might be different between developers. For example, you might not want to pull the godot engine down all the time and instead share a copy you have on your computer using the `local` parameter on the Godot option in your `project.yml` file.

However, one person might be using a Mac version of Godot, whereas another person might be using Windows. The replacement Tokens are specified in your `.gspm` settings file in your Home folder.

For more information on this file, read the documentation on :doc:`settings<settings>`.

A Token, is specified in the `tokens` section of your file, and are specified using a String value, surrounded by dollar signs. For example;

.. code-block:: yaml

    name: my-cool-game
    description: My Very Cool Game
    default_type: git
    godot:
      local: $godot_306$

In your configuration file, you might have this;

.. code-block:: ini

    [tokens]
    $godot_306$=c:\godot-3.0.6\godot.exe
    $godot_31a$=c:\godot-3.1a1\godot.exe

When the tool executes, it will search through all replacement Tokens in your configuration file, and will replace them in your project.yml file before it begins running the command you have requested.

In the example above, the project file will be changed to this before it executes.

.. code-block:: yaml

    name: my-cool-game
    description: My Very Cool Game
    default_type: git
    godot:
      local: c:\godot-3.0.6\godot.exe
