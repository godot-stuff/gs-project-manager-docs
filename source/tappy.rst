Tappy Plane Example
===================
As a simple way to show how to use the tool, we have taken the Tappy Plane example project and converted it to use `gspm`.

Before starting, you should make sure you have :doc:`installed<install>` the tool. You will also need to be familiar with GIT, and make sure that it is installed as well.

Clone The Project
-----------------
Use git to Clone the project to a directory of your choice.

.. code-block:: bash

    > git clone https://gitlab.com/godot-stuff/gs-tappy-plane.git

Change To Project Directory
---------------------------
It is usally best to execute commands from within the folder of the project where your configuration file exists. There are alternatives, but those are more advanced.

.. code-block:: bash

    > cd gs-tappy-plane 

Install Project Components
--------------------------
Before you start working on your project you need to install the components of your project. This would include the version of Godot you need and also any additional Assets that you have added to your project configuration.

.. code-block:: bash

    > gspm install

Edit The Project
----------------
You can start the Editor from the command line by using the `Edit` command. This will run the version of Godot you specified in the project and will open the Editor automatically.

.. code-block:: bash

    > gspm edit

Run The Project
---------------
Sometimes you just want test the project without going into the Editor. This can be done by using the `Run` command.

.. tip::

    You must make sure you Edit the project at least once before trying to Run it. This will ensure that all resources are imported correctly.

.. code-block:: bash

    > gspm run







