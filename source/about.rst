About
=====
Thanks for your interest in the godot-stuff Program Manager.

This is a multi-platform Command Line Tool (CLI) that can be used to help you manage your Godot projects.

Project information is controlled using a plain text file in YAML that defines the blue prints of your project.

It stops the problem of wondering what version of Godot you used, and what assets are needed in your projects while at the same time letting developers on different platforms work together.

I started this project to fill a gap that I felt was present in the management of projects for my Games and Applications built with Godot. I come from a heavy Java/C# coding background where we have many tools at our disposal to assist in building and maintaining projects.

This is my crazy attempt to take some of those core ideas, specifically code management, and code re-use with predictable project building and testing, and make them a core to my Development Workflow.

I must warn you, this is something to scratch my itch, and it won't be for everyone. I am putting it out here in hopes that people contribute and make the tool better.

If you can't wait and want to get started immediately, please have a look at :doc:`install`

Principles
----------
These are the principals behind this tool

* **Be Modular** - using previously built components when possible speeds development. Packages from the Asset Library are an example of a Module. Code libraries are also an example of modules. Artwork, to some extent can also be modular.

* **Be Automated** - working on your project from one computer to another, or on multiple machines should be automated to avoid any human error when possible.

* **Be Clean** - setting up a project should follow the "nuke and pave" paradigm, meaning that it can always be setup from a clean state.

* **Be Traceable** - your project configuration changes with you, and version control can be used to historically tell the story of the project.

* **Be Simple** - it should be easy to use, but robust enough for people to have full control over their entire project ecosystem.

* **Be Team Friendly** - team members working on multiple parts of the project should be transparent. The tool should be flexible to ensure that slight changes to one members environment does not cause pain for another. An example of this is the type of computer a developer is using. The tool should work the same on Linux as it does on a Mac or PC.
