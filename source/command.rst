Command
=======

This tool is controlled exclusively from a command line. The example given will usually show commands running in
a Windows environment, but they will do the same thing on the Mac and in Linux.

**gspm** [options] <command>

Options
-------

.. option:: -V, --version

Show the Version and Exit.

.. option:: -h, --help

Show the Help and Exit.

.. option:: -c, --config {CONFIG}

Use a specific Configuration file. The default is **project.yml**.

.. option:: -f, --force

Force a command to execute with Warnings. Use this when you are sure of what you are doing.

.. option:: --verbose, --more-verbose

Change the level of messages generated when executing.

.. option:: --quiet

Only the most important Messages, Errors and Warnings will show. The logo will also be supressed.


Commands
--------

Clean
+++++
Remove all assets from the Project and Repository, but leave the Godot executable in the repository. This is useful when you want to reinstall your assets if you know there are updates.

When the **addons** folder is empty after cleaning, it will also be removed from the project folder.

.. note::

    Inactive assets are **not** included in this cleanup.

.. note::

    When you have removed an asset from your project configuration, it will not be able to clean it up, and you will have to do this manually.


Edit
++++
Starts the GODOT editor and opens your project.

Export
++++++
Exports your project with the configuration specified.

.. option:: name

The name of the export you want to use from your configuration file.


Install
+++++++
Pulls Godot and the other Assets specified in the projects configuration file.

.. option:: --headless

When installing, use the headless version of Godot. This is useful when you want to build your game from a continuous integration server.
    
New
+++
Create a new project for Godot.

.. option:: -t {name}, --template {name}

   Use a Template when creating new project.

Run
+++
Run the project using the Startup scene.

Test
++++
Run unit tests for your project.

Update
++++++
Update your project assets.


Examples
--------

.. code-block:: batch

    # edit using the configuration file my_project.yml
    gspm -c my_project.yml edit

