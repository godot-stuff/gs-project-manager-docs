Settings
========
The settings file is a basic configuration or `.ini` file which contains a number of sections, followed by a number of key value pairs.

Location
--------
When `gspm` starts up, it will look in these locations for a file called `.gspm`. If no file is found, it will simply use the default settings that are programmed into the tool.

On Windows, this is usually located in the value of the environment variable `%USERPROFILE%`, on Windows 10, this is at

.. code-block:: cmd

    c:\users\username


For Mac and Linux users, this is usually the value in your `$HOME` environment variable, sometimes at

.. code-block:: bash

    /home/username


General Section
---------------
This section contains a number of basic settings that are used when you create a project using the NEW :doc:`command<command>`.

.. code-block:: ini

    [general]
    author=sP0CkEr2
    copyright=Copyright 2020, SpockerDotNet LLC.
    email=paul@spocker.net
    twitter=https://twitter.com/sP0CkEr2
    license=MIT


Godot Section
-------------
When creating a new projects, the settings in this section will control what version of Godot you want to use. Refer to the :doc:`project<project>` configuration documentation for information on what values you can place here.

.. code-block:: ini 

    [godot]
    version=3.2.3
    arch=64
    mono=false

Tokens Section 
--------------
You can add replacement tokens in this section of the settings file. Remember that replacement tokens can be use `anywhere` in your settings file, but are mostly used to control the `local` setting of the godot engine configuration in your projects.

.. code-block:: ini 

    [tokens]
    $godot_214$=c:\users\paul\development\tools\godot\godot-2.1.4\godot.exe
    $godot_323$=c:\users\paul\development\tools\godot\godot-3.2.3.exe
    $214$=c:\users\paul\development\tools\godot\godot-2.1.4\godot.exe
    $306$=c:\users\paul\development\tools\godot\godot-3.0.6\godot.exe
    $311$=c:\users\paul\development\tools\godot\godot-3.1.1\godot.exe
    $323$=c:\users\paul\development\tools\godot\godot-3.2.3.exe
    $build$=c:\users\paul\workspace\godot\bin\godot.windows.tools.64.exe
