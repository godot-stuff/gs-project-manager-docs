gs-project-manager Docs - *Master Branch*
=========================================

.. tip::

    This is documentation for the development (master) branch. Looking for documentation of the current stable branch? Have a look here.

Welcome to the official documentation of the gs-project-manager. A cross-platform tool, written
in Python, to help you manage your Godot projects consistently and easily.

The table of contents below and in the sidebar should let you easily 
access the documentation for your topic of interest. You can also use
the search function in the top left corner.

.. toctree::
   :maxdepth: 1
   :caption: General

   About <about.rst>

.. toctree::
   :maxdepth: 1
   :caption: Getting Started 

   Installation <install.rst>
   Tappy Plane Example <tappy.rst>
   

.. toctree::
   :maxdepth: 1
   :caption: Reference

   Command Line <command.rst>
   Project <project.rst>
   Schema <schema.rst>
   Settings <settings.rst>

 